/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;


namespace XploRe.Configuration.Yaml.Tests
{

    public class YamlDocumentGenerator
    {

        /// <summary>
        ///     Yields YAML streams that represent invalid configuration files.
        /// </summary>
        public static IEnumerable<object[]> InvalidStreams
        {
            [UsedImplicitly]
            get {
                yield return new object[] { "/" };
                yield return new object[] { "42" };
                yield return new object[] { "- a" };
                yield return new object[] { "null" };
                yield return new object[] { "test" };
            }
        }

        /// <summary>
        ///     Yields valid YAML documents with their corresponding index and output dictionary.
        /// </summary>
        public static IEnumerable<object[]> ValidDocumentsWithIndex
        {
            [UsedImplicitly]
            get {
                // Empty stream.
                yield return new object[] { string.Empty, 0, new Dictionary<string, string>(0) };
                // Empty stream, non-existing document (yields empty dictionary).
                yield return new object[] { string.Empty, 1, new Dictionary<string, string>(0) };

                // Simple stream.
                yield return new object[] { "test: 42", 0, new Dictionary<string, string> { { "test", "42" } } };
                // Simple stream, non-existing document (yields empty dictionary).
                yield return new object[] { "test: 42", 1, new Dictionary<string, string>(0) };

                var complex = @"# Complex example.
one: 1
two: 2
three:
    sub: node
    list:
        - a
        - b

---

three: 3
four: 4
five:
    six: 6
    seven: 7

---

eight: 8";

                // Complex stream, first document.
                yield return new object[] {
                    complex, 0,
                    new Dictionary<string, string> {
                        { ConfigurationPath.Combine("one"), "1" },
                        { ConfigurationPath.Combine("two"), "2" },
                        { ConfigurationPath.Combine("three", "sub"), "node" },
                        { ConfigurationPath.Combine("three", "list", "0"), "a" },
                        { ConfigurationPath.Combine("three", "list", "1"), "b" }
                    }
                };

                // Complex stream, second document.
                yield return new object[] {
                    complex, 1,
                    new Dictionary<string, string> {
                        { ConfigurationPath.Combine("three"), "3" },
                        { ConfigurationPath.Combine("four"), "4" },
                        { ConfigurationPath.Combine("five", "six"), "6" },
                        { ConfigurationPath.Combine("five", "seven"), "7" }
                    }
                };

                // Complex stream, third document.
                yield return new object[] {
                    complex, 2,
                    new Dictionary<string, string> {
                        { ConfigurationPath.Combine("eight"), "8" },
                    }
                };
            }
        }

    }

}
