/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Text;
using FluentAssertions;
using JetBrains.Annotations;
using XploRe.Runtime;
using Xunit;


namespace XploRe.Configuration.Yaml.Tests
{

    public class YamlConfigurationStreamParserTest
    {

        [Theory]
        [MemberData(nameof(YamlDocumentGenerator.InvalidStreams), MemberType = typeof(YamlDocumentGenerator))]
        public void ParseInvalid([NotNull] string yamlText)
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            var stream = yamlText.ToMemoryStream(encoding: Encoding.UTF8);
            var parser = new YamlConfigurationStreamParser();

            Action parse = () => parser.Parse(stream);

            // TODO: Needs to be updated to check for exceptionType once FluentAssertions supports generic parameters.
            parse.ShouldThrowExactly<FormatException>();
        }

        [Theory]
        [MemberData(nameof(YamlDocumentGenerator.ValidDocumentsWithIndex), MemberType = typeof(YamlDocumentGenerator))]
        public void ParseWithDocumentIndex(
            [NotNull] string yamlText,
            int documentIndex,
            [NotNull] IDictionary<string, string> expected)
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            var stream = yamlText.ToMemoryStream(Encoding.UTF8);
            var parser = new YamlConfigurationStreamParser();

            parser.Parse(stream, documentIndex);

            var parsed = parser.Data;

            parsed.Should().NotBeNull();
            parsed.ShouldBeEquivalentTo(expected);
        }

    }

}
