﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;


namespace XploRe.Configuration
{

    /// <inheritdoc />
    /// <summary>
    ///     Represents a YAML document as an <see cref="T:Microsoft.Extensions.Configuration.IConfigurationSource" />.
    /// </summary>
    public class YamlConfigurationSource : FileConfigurationSource
    {

        /// <summary>
        ///     The zero-based YAML document index to read the configuration from. If the document does not exist, it is
        ///     treated as an empty configuration file.
        /// </summary>
        public int DocumentIndex { get; set; }

        /// <inheritdoc />
        /// <summary>
        ///     Builds the <see cref="T:XploRe.Configuration.YamlConfigurationSource" /> for the configured source.
        /// </summary>
        /// <param name="builder">The <see cref="T:Microsoft.Extensions.Configuration.IConfigurationBuilder" /> instance.</param>
        /// <returns>A new <see cref="T:XploRe.Configuration.YamlConfigurationSource" /> instance.</returns>
        public override IConfigurationProvider Build([NotNull] IConfigurationBuilder builder)
        {
            // Set defaults such as the file provider from the builder.
            EnsureDefaults(builder);

            return new YamlConfigurationProvider(this);
        }

    }

}
