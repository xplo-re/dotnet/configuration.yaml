﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Runtime.CompilerServices;


[assembly: InternalsVisibleTo("XploRe.Configuration.Yaml.Tests")]
