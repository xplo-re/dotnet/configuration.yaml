﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.IO;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;


namespace XploRe.Configuration
{

    /// <summary>
    ///     Provides <see cref="IConfigurationBuilder" /> extension methods to register YAML configuration files.
    /// </summary>
    public static class YamlConfigurationExtensions
    {

        /// <summary>
        ///     Adds a YAML configuration source to <paramref name="builder" />.
        /// </summary>
        /// <param name="builder">
        ///     The <see cref="IConfigurationBuilder" /> instance to add a YAML configuration source to.
        /// </param>
        /// <param name="path">
        ///     The YAML configuration file path relative to the base path stored in 
        ///     <see cref="IConfigurationBuilder.Properties" /> of <paramref name="builder" />.
        /// </param>
        /// <returns>The <see cref="IConfigurationBuilder" /> instance.</returns>
        [NotNull]
        public static IConfigurationBuilder AddYamlFile(
            [NotNull] this IConfigurationBuilder builder,
            [NotNull] string path)
            => AddYamlFile(builder, provider: null, path: path, optional: false, reloadOnChange: false);

        /// <summary>
        ///     Adds a YAML configuration source to <paramref name="builder" />.
        /// </summary>
        /// <param name="builder">
        ///     The <see cref="IConfigurationBuilder" /> instance to add a YAML configuration source to.
        /// </param>
        /// <param name="path">
        ///     The YAML configuration file path relative to the base path stored in 
        ///     <see cref="IConfigurationBuilder.Properties" /> of <paramref name="builder" />.
        /// </param>
        /// <param name="documentIndex">The YAML document index to read configuration from.</param>
        /// <returns>The <see cref="IConfigurationBuilder" /> instance.</returns>
        [NotNull]
        public static IConfigurationBuilder AddYamlFile(
            [NotNull] this IConfigurationBuilder builder,
            [NotNull] string path,
            int documentIndex)
            => AddYamlFile(
                builder,
                provider: null, path: path, documentIndex: documentIndex, optional: false, reloadOnChange: false);

        /// <summary>
        ///     Adds a YAML configuration source to <paramref name="builder" />.
        /// </summary>
        /// <param name="builder">
        ///     The <see cref="IConfigurationBuilder" /> instance to add a YAML configuration source to.
        /// </param>
        /// <param name="path">
        ///     The YAML configuration file path relative to the base path stored in 
        ///     <see cref="IConfigurationBuilder.Properties" /> of <paramref name="builder" />.
        /// </param>
        /// <param name="optional">
        ///     Controls whether the configuration file is optional. If set to <c>false</c> (the default), a
        ///     <see cref="FileNotFoundException" /> is thrown if the configuration file is missing.
        /// </param>
        /// <returns>The <see cref="IConfigurationBuilder" /> instance.</returns>
        [NotNull]
        public static IConfigurationBuilder AddYamlFile(
            [NotNull] this IConfigurationBuilder builder,
            [NotNull] string path,
            bool optional)
            => AddYamlFile(builder, provider: null, path: path, optional: optional, reloadOnChange: false);

        /// <summary>
        ///     Adds a YAML configuration source to <paramref name="builder" />.
        /// </summary>
        /// <param name="builder">
        ///     The <see cref="IConfigurationBuilder" /> instance to add a YAML configuration source to.
        /// </param>
        /// <param name="path">
        ///     The YAML configuration file path relative to the base path stored in 
        ///     <see cref="IConfigurationBuilder.Properties" /> of <paramref name="builder" />.
        /// </param>
        /// <param name="documentIndex">The YAML document index to read configuration from.</param>
        /// <param name="optional">
        ///     Controls whether the configuration file is optional. If set to <c>false</c> (the default), a
        ///     <see cref="FileNotFoundException" /> is thrown if the configuration file is missing.
        /// </param>
        /// <returns>The <see cref="IConfigurationBuilder" /> instance.</returns>
        [NotNull]
        public static IConfigurationBuilder AddYamlFile(
            [NotNull] this IConfigurationBuilder builder,
            [NotNull] string path,
            int documentIndex,
            bool optional)
            => AddYamlFile(
                builder,
                provider: null, path: path, documentIndex: documentIndex, optional: optional, reloadOnChange: false);

        /// <summary>
        ///     Adds a YAML configuration source to <paramref name="builder" />.
        /// </summary>
        /// <param name="builder">
        ///     The <see cref="IConfigurationBuilder" /> instance to add a YAML configuration source to.
        /// </param>
        /// <param name="path">
        ///     The YAML configuration file path relative to the base path stored in 
        ///     <see cref="IConfigurationBuilder.Properties" /> of <paramref name="builder" />.
        /// </param>
        /// <param name="optional">
        ///     Controls whether the configuration file is optional. If set to <c>false</c> (the default), a
        ///     <see cref="FileNotFoundException" /> is thrown if the configuration file is missing.
        /// </param>
        /// <param name="reloadOnChange">
        ///     Controls whether the source should be automatically reloaded if a change to the underlying configuration
        ///     file has been detected.
        /// </param>
        /// <returns>The <see cref="IConfigurationBuilder" /> instance.</returns>
        [NotNull]
        public static IConfigurationBuilder AddYamlFile(
            [NotNull] this IConfigurationBuilder builder,
            [NotNull] string path,
            bool optional, bool reloadOnChange)
            => AddYamlFile(builder, provider: null, path: path, optional: optional, reloadOnChange: reloadOnChange);

        /// <summary>
        ///     Adds a YAML configuration source to <paramref name="builder" />.
        /// </summary>
        /// <param name="builder">
        ///     The <see cref="IConfigurationBuilder" /> instance to add a YAML configuration source to.
        /// </param>
        /// <param name="path">
        ///     The YAML configuration file path relative to the base path stored in 
        ///     <see cref="IConfigurationBuilder.Properties" /> of <paramref name="builder" />.
        /// </param>
        /// <param name="documentIndex">The YAML document index to read configuration from.</param>
        /// <param name="optional">
        ///     Controls whether the configuration file is optional. If set to <c>false</c> (the default), a
        ///     <see cref="FileNotFoundException" /> is thrown if the configuration file is missing.
        /// </param>
        /// <param name="reloadOnChange">
        ///     Controls whether the source should be automatically reloaded if a change to the underlying configuration
        ///     file has been detected.
        /// </param>
        /// <returns>The <see cref="IConfigurationBuilder" /> instance.</returns>
        [NotNull]
        public static IConfigurationBuilder AddYamlFile(
            [NotNull] this IConfigurationBuilder builder,
            [NotNull] string path,
            int documentIndex,
            bool optional, bool reloadOnChange)
            => AddYamlFile(
                builder,
                provider: null, path: path, documentIndex: documentIndex,
                optional: optional, reloadOnChange: reloadOnChange);

        /// <summary>
        ///     Adds a YAML configuration source to <paramref name="builder" />.
        /// </summary>
        /// <param name="builder">
        ///     The <see cref="IConfigurationBuilder" /> instance to add a YAML configuration source to.
        /// </param>
        /// <param name="provider">
        ///     The <see cref="IFileProvider" /> instance to use to access the contents of the YAML configuration file.
        /// </param>
        /// <param name="path">
        ///     The YAML configuration file path relative to the base path stored in 
        ///     <see cref="IConfigurationBuilder.Properties" /> of <paramref name="builder" />.
        /// </param>
        /// <param name="optional">
        ///     Controls whether the configuration file is optional. If set to <c>false</c> (the default), a
        ///     <see cref="FileNotFoundException" /> is thrown if the configuration file is missing.
        /// </param>
        /// <param name="reloadOnChange">
        ///     Controls whether the source should be automatically reloaded if a change to the underlying configuration
        ///     file has been detected.
        /// </param>
        /// <returns>The <see cref="IConfigurationBuilder" /> instance.</returns>
        [NotNull]
        public static IConfigurationBuilder AddYamlFile(
            [NotNull] this IConfigurationBuilder builder,
            [CanBeNull] IFileProvider provider,
            [NotNull] string path,
            bool optional, bool reloadOnChange)
            => AddYamlFile(
                builder,
                provider: provider, path: path, documentIndex: 0, optional: optional, reloadOnChange: reloadOnChange);

        /// <summary>
        ///     Adds a YAML configuration source to <paramref name="builder" />.
        /// </summary>
        /// <param name="builder">
        ///     The <see cref="IConfigurationBuilder" /> instance to add a YAML configuration source to.
        /// </param>
        /// <param name="provider">
        ///     The <see cref="IFileProvider" /> instance to use to access the contents of the YAML configuration file.
        /// </param>
        /// <param name="path">
        ///     The YAML configuration file path relative to the base path stored in 
        ///     <see cref="IConfigurationBuilder.Properties" /> of <paramref name="builder" />.
        /// </param>
        /// <param name="documentIndex">The YAML document index to read configuration from.</param>
        /// <param name="optional">
        ///     Controls whether the configuration file is optional. If set to <c>false</c> (the default), a
        ///     <see cref="FileNotFoundException" /> is thrown if the configuration file is missing.
        /// </param>
        /// <param name="reloadOnChange">
        ///     Controls whether the source should be automatically reloaded if a change to the underlying configuration
        ///     file has been detected.
        /// </param>
        /// <returns>The <see cref="IConfigurationBuilder" /> instance.</returns>
        [NotNull]
        public static IConfigurationBuilder AddYamlFile(
            [NotNull] this IConfigurationBuilder builder,
            [CanBeNull] IFileProvider provider,
            [NotNull] string path,
            int documentIndex,
            bool optional, bool reloadOnChange)
        {
            if (null == builder) {
                throw new ArgumentNullException(nameof(builder));
            }

            if (null == path) {
                throw new ArgumentNullException(nameof(path));
            }

            if (string.IsNullOrEmpty(path)) {
                throw new ArgumentException("File path cannot be empty.", nameof(path));
            }

            var source = new YamlConfigurationSource {
                FileProvider = provider,
                Path = path,
                DocumentIndex = documentIndex,
                Optional = optional,
                ReloadOnChange = reloadOnChange
            };

            builder.Add(source);

            return builder;
        }

        /// <summary>
        ///     Adds a YAML configuration source to <paramref name="builder" />.
        /// </summary>
        /// <param name="builder">
        ///     The <see cref="IConfigurationBuilder" /> instance to add a YAML configuration source to.
        /// </param>
        /// <param name="configurationSource">Configures the source.</param>
        /// <returns>The <see cref="IConfigurationBuilder" /> instance.</returns>
        [NotNull]
        public static IConfigurationBuilder AddYamlFile(
            [NotNull] this IConfigurationBuilder builder,
            [NotNull] Action<YamlConfigurationSource> configurationSource)

        {
            if (builder == null) {
                throw new ArgumentNullException(nameof(builder));
            }

            if (configurationSource == null) {
                throw new ArgumentNullException(nameof(configurationSource));
            }

            var source = new YamlConfigurationSource();

            configurationSource.Invoke(source);
            builder.Add(source);

            return builder;
        }

    }

}
