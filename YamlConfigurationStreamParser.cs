﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using XploRe.Runtime;
using YamlDotNet.RepresentationModel;


namespace XploRe.Configuration
{

    /// <summary>
    ///     Parses streams of YAML configuration options.
    /// </summary>
    internal class YamlConfigurationStreamParser
    {

        /// <summary>
        ///     All key-value pairs of configuration values (with absolute paths) that were read from the parsed
        ///     configuration YAML documents.
        /// </summary>
        [NotNull]
        public IDictionary<string, string> Data { get; }
            = new SortedDictionary<string, string>(StringComparer.OrdinalIgnoreCase);


        #region Parser 

        /// <summary>
        ///     Parses a given YAML configuration stream and adds all configuration values to the key-value dictionary
        ///     <see cref="Data" />.
        /// </summary>
        /// <param name="stream"><see cref="Stream" /> instance that holds YAML configuration data.</param>
        /// <param name="documentIndex">
        ///     Zero-based index of the YAML document to parse. Defaults to the first document at index 0. If the index
        ///     is negative, a <see cref="ArgumentOutOfRangeException" /> is thrown. Otherwise, if the document index
        ///     is out of range, the non-existing document is treated as an empty configuration section.
        /// </param>
        public void Parse([NotNull] Stream stream, int documentIndex = 0)
        {
            if (stream == null) {
                throw new ArgumentNullException(nameof(stream));
            }

            if (documentIndex < 0) {
                throw new ArgumentOutOfRangeException(nameof(documentIndex), "Document index must be non-negative.");
            }

            var yamlStream = new YamlStream();

            using (var streamReader = new StreamReader(stream)) {
                yamlStream.Load(streamReader);
            }

            var documents = yamlStream.Documents;

            if (documents == null) {
                throw RuntimeInconsistencyException.FromUnexpectedNull(() => yamlStream.Documents);
            }

            if (!documents.Any() || documentIndex >= documents.Count) {
                // Either file is empty (has no documents) or the requested document does not exist, i.e. no options
                // found in source.
                return;
            }

            var document = documents[documentIndex];

            if (document == null) {
                throw new RuntimeInconsistencyException(
                    $"Document at index {documentIndex} in YAML stream is null."
                );
            }

            if (document.RootNode == null) {
                throw new RuntimeInconsistencyException(
                    $"Root node of document at index {documentIndex} in YAML stream is null."
                );
            }

            if (!(document.RootNode is YamlMappingNode rootNode)) {
                throw new FormatException(
                    "Root node in YAML configuration stream must be a mapping node," +
                    " got '{0}' instead.\n{1}.".FormatWith(document.RootNode, document.RootNode.Start)
                );
            }

            ParseMappingNode(rootNode);
        }

        /// <summary>
        ///     Processes a YAML key-value mapping node.
        /// </summary>
        /// <param name="node"><see cref="YamlMappingNode" /> instance to process.</param>
        private void ParseMappingNode([NotNull] YamlMappingNode node)
        {
            Debug.Assert(node != null, $"{nameof(node)} != null");

            var children = node.Children;

            if (children == null) {
                throw new RuntimeInconsistencyException(
                    $"YAML node {nameof(node.Children)} property is null at key path '{_currentPath}'."
                );
            }

            foreach (var item in children) {
                var key = (string) item.Key;

                if (key == null) {
                    throw new FormatException($"Invalid null key encountered under parent key path '{_currentPath}'.");
                }

                EnterContext(key);
                ParseNode(item.Value);
                ExitContext();
            }
        }

        /// <summary>
        ///     Processes a YAML sequence node.
        /// </summary>
        /// <param name="node"><see cref="YamlSequenceNode" /> instance to process.</param>
        private void ParseSequenceNode([NotNull] YamlSequenceNode node)
        {
            Debug.Assert(node != null, $"{nameof(node)} != null");

            var children = node.Children;

            if (children == null) {
                throw new RuntimeInconsistencyException(
                    $"YAML node {nameof(node.Children)} property is null at key path '{_currentPath}'."
                );
            }

            for (var i = 0; i < children.Count; ++i) {
                var key = i.ToString("D", CultureInfo.InvariantCulture);

                if (key == null) {
                    throw RuntimeInconsistencyException.FromUnexpectedNull(() => i.ToString());
                }

                EnterContext(key);
                ParseNode(children[i]);
                ExitContext();
            }
        }

        /// <summary>
        ///     Stores the YAML node to the output dictionary.
        /// </summary>
        /// <param name="node"><see cref="YamlScalarNode" /> instance to process.</param>
        private void ParseScalarNode([NotNull] YamlScalarNode node)
        {
            Debug.Assert(node != null, $"{nameof(node)} != null");

            var key = _currentPath;

            if (Data.ContainsKey(key)) {
                throw new FormatException($"Duplicate configuration key '{key}'.");
            }

            Data[key] = node.ToString();
        }

        /// <summary>
        ///     Processes a YAML node recursively.
        /// </summary>
        /// <param name="node"><see cref="YamlNode" /> instance to process.</param>
        private void ParseNode([CanBeNull] YamlNode node)
        {
            switch (node) {
            case YamlScalarNode scalarNode:
                ParseScalarNode(scalarNode);
                break;

            case YamlMappingNode mappingNode:
                ParseMappingNode(mappingNode);
                break;

            case YamlSequenceNode sequenceNode:
                ParseSequenceNode(sequenceNode);
                break;

            default:
                throw new FormatException(
                    $"Encountered invalid YAML node of type {node?.NodeType} at key path '{_currentPath}'."
                );
            }
        }

        #endregion


        #region Context

        /// <summary>
        ///     Context stack for nested configuration options.
        /// </summary>
        [NotNull]
        private readonly Stack<string> _context = new Stack<string>();

        /// <summary>
        ///     Current configuration key path, used by recursive parser calls.
        /// </summary>
        [NotNull]
        private string _currentPath = string.Empty
                                      ?? throw RuntimeInconsistencyException.FromUnexpectedNull(() => string.Empty);

        /// <summary>
        ///     Pushes the nested <paramref name="context" /> configuration key to the context stack and updates the
        ///     internal path cache.
        /// </summary>
        /// <param name="context">Nested configuration key.</param>
        private void EnterContext([NotNull] string context)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            _context.Push(context);

            var path = ConfigurationPath.Combine(_context.Reverse());

            if (path == null) {
                throw new RuntimeException("Failed to construct configuration path, method returned null.");
            }

            _currentPath = path;
        }

        /// <summary>
        ///     Pops the last configuration key from the context stack and updates the internal path cache.
        /// </summary>
        private void ExitContext()
        {
            _context.Pop();

            var path = ConfigurationPath.Combine(_context.Reverse());

            if (path == null) {
                throw new RuntimeException("Failed to construct configuration path, method returned null.");
            }

            _currentPath = path;
        }

        #endregion

    }

}
