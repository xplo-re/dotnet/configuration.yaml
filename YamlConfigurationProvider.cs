﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.IO;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using XploRe.Diagnostics;
using XploRe.Runtime;
using YamlDotNet.Core;


namespace XploRe.Configuration
{

    /// <inheritdoc />
    /// <summary>
    ///     YAML-based configuration provider.
    /// </summary>
    public class YamlConfigurationProvider : FileConfigurationProvider
    {

        // ReSharper disable once SuggestBaseTypeForParameter
        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Configuration.YamlConfigurationProvider" /> instance with the
        ///     specified source.
        /// </summary>
        /// <param name="source">The configuration source.</param>
        public YamlConfigurationProvider([NotNull] YamlConfigurationSource source) : base(source)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Loads and parses the YAML configuration from a given <see cref="T:System.IO.Stream" />.
        /// </summary>
        /// <param name="stream">
        ///     The <see cref="T:System.IO.Stream" /> instance to load and parse the YAML configuration from.
        /// </param>
        public override void Load([NotNull] Stream stream)
        {
            if (stream == null) {
                throw new ArgumentNullException(nameof(stream));
            }

            var parser = new YamlConfigurationStreamParser();
            var source = Source as YamlConfigurationSource;

            if (source == null) {
                throw RuntimeInconsistencyException.FromUnexpectedNull(() => source);
            }

            try {
                parser.Parse(stream, source.DocumentIndex);

                Data = parser.Data;
            }
            catch (YamlException e) {
                throw new FormatException(
                    "Failed to parse YAML configuration.".AppendDiagnosticsSentence(e.Message),
                    e
                );
            }
        }

    }

}
